#pragma once
#include "object.hpp"

namespace el
{
    enum class Level
    {
        High,
        Low
    };

    class Bottle : public Object
    {
    public:
        Bottle(int x, int y);
        virtual ~Bottle();
        bool Setup() override;
        void SetHighLevel();
        void SetLowLevel();
        void Move(int timeMs) override;

    private:
        sf::Texture e_revHighTexture_1;
        sf::Texture e_revHighTexture_2;
        sf::Texture e_revLowTexture_1;
        sf::Texture e_revLowTexture_2;
        Level e_level = Level::Low;
        int e_animateIndex = 0;

    };
}