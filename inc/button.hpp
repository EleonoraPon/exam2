#pragma once
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

namespace el
{

	class Button
	{
	public:
		Button(int x, int y);
		~Button();
		bool Setup();
		void SetColorRed();
		void SetColorGreen();
		std::shared_ptr<sf::RectangleShape> Get() { return e_button; }

	private:
		int e_x, e_y;
		std::shared_ptr<sf::RectangleShape> e_button;
		sf::Texture e_textureButton_1;
		sf::Texture e_textureButton_2;
	};
}