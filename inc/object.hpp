#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace el
{
	struct Constituents//������������ 
	{
		bool res1_high; //������
		bool res2_high;
		bool res1_low;
		bool res2_low;
		bool pumpwork;
	};

	class Object
	{
	public:
		Object(int x, int y);
		virtual ~Object();
		virtual bool Setup();
		virtual void Move(int timeMs) = 0;
		std::shared_ptr<sf::Sprite> Get() { return e_shape; } 


	protected:
		int e_w, e_h;
		int e_x, e_y;
		sf::Texture e_texture;
		std::shared_ptr <sf::Sprite> e_shape;

	};



}