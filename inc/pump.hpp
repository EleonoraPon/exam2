#pragma once
#include "object.hpp"

namespace el
{
	class Pump : public Object
	{
	public:
		Pump(int x, int y);
		virtual ~Pump();
		bool Setup() override;
		void Move(int timeMs) override;


	private:
		int e_angle = 0;
		//std::unique_ptr<sf::Sprite> e_shape;
		//sf::Texture e_texture;
		//std::unique_ptr <sf::Sprite> e_shape;
	};
}