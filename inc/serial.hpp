#pragma once
#include <asio.hpp>
#include <iostream>
#include <object.hpp>

namespace el
{
	class Serial
	{
	public:
		Serial(std::string port);
		~Serial();
		void Update(Constituents& constituents);

	private:
		asio::io_service m_io;
		asio::serial_port m_serial;
		bool m_send = true;
	};
}