﻿#include <SFML/Graphics.hpp>
//#include <SFML/Network/.hpp>
#include <chrono>
#include "pump.hpp"
#include "bottle.hpp"
#include "button.hpp"
#include <chrono>
#include <thread>
#include <serial.hpp>

using namespace std::chrono_literals;


int main()
{
    srand(time(0));

    sf::RenderWindow window(sf::VideoMode(1300, 900), "Control Panel");

    el::Constituents con = { 0, 0, 1, 1, 0 };


    auto pump = std::make_unique<el::Pump>(650, 150);
    pump->Setup();

    auto bottle_1 = std::make_unique<el::Bottle>(200, 300); 
    bottle_1->Setup();

    auto bottle_2 = std::make_unique<el::Bottle>(1100, 300);
    bottle_2->Setup();

    auto buttonG = std::make_unique<el::Button>(500, 500);
    buttonG->Setup();
    
    auto buttonR = std::make_unique<el::Button>(500, 700);
    buttonR->Setup();

    el::Serial serial("COM3");
    

    int globalTimeMs = 0;

    while (window.isOpen())
    {
        sf::Event event;
        

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        

        //if(sf::Mouse::isButtonPressed(sf::Mouse::Left) and )

        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i pos = sf::Mouse::getPosition(window);
            //if(buttonG.getGlobalBounds().contains(pos.x, pos.y))
            if ((pos.x > 500 && pos.x < 800) && (pos.y > 500 && pos.y < 600))
            {
                con.pumpwork = true;
            }
            else if ((pos.x > 500 && pos.x < 800) && (pos.y > 700 && pos.y < 800))
            {
                con.pumpwork = false;
            }
        }

        if (con.pumpwork)
        {
            pump->Move(globalTimeMs);
            bottle_1->Move(globalTimeMs);
            bottle_2->Move(globalTimeMs);
        }
        buttonG->SetColorGreen(); 
        buttonR->SetColorRed();

        
        serial.Update(con);
        if (con.res1_high)
        {
            bottle_1->SetHighLevel();
            con.pumpwork = false;
        }
        if (con.res1_low)
            bottle_1->SetLowLevel();
        if (con.res2_high)
        {
            bottle_2->SetHighLevel();
            //con.pumpwork = false;
        }
        if (con.res2_low)
            bottle_2->SetLowLevel();

        window.clear({ 255, 255, 255 });
        window.draw(*(pump->Get()));
        window.draw(*(bottle_1->Get()));
        window.draw(*(bottle_2->Get()));
        window.draw(*(buttonG->Get()));
        window.draw(*(buttonR->Get()));

        window.display();

        //serial.Update(con);

        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        globalTimeMs += 20;

    }

    return 0;
}