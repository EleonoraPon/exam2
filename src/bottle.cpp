#include "bottle.hpp"

namespace el
{
	
	Bottle::Bottle(int x, int y) : Object(x, y)
	{

	}
	Bottle::~Bottle()
	{

	}

	bool Bottle::Setup()
	{
		if (!e_revHighTexture_1.loadFromFile("img/rev_high1.png") ||
			!e_revHighTexture_2.loadFromFile("img/rev_high2.png") ||
			!e_revLowTexture_1.loadFromFile("img/rev_low1.png") ||
			!e_revLowTexture_2.loadFromFile("img/rev_low2.png"))
		{
			std::cout << "ERROR: one of the pictures with the water level was not found" << std::endl;
			return false;
		}

		e_revHighTexture_1.setSmooth(true);
		e_revHighTexture_2.setSmooth(true);
		e_revLowTexture_1.setSmooth(true);
		e_revLowTexture_2.setSmooth(true);

		e_shape = std::make_unique <sf::Sprite>();//������� ���������
		e_shape->setTexture(e_revLowTexture_1);//
		e_shape->setOrigin(static_cast<float>(e_revLowTexture_1.getSize().x / 2), static_cast<float>(e_revLowTexture_1.getSize().y / 2)); 
		                                                                                                //���������� ����� ������ (� �����) 
		e_shape->setPosition(e_x, e_y);//����� ������������ ��������

		return true;
	}

	void Bottle::SetHighLevel() { e_level = Level::High; }

	void Bottle::SetLowLevel() { e_level = Level::Low; }

	void Bottle::Move(int timeMs)
	{
		if (timeMs % 100 == 0) {
			e_animateIndex++;
			if (e_animateIndex > 1)
				e_animateIndex = 0;

			if (e_level == Level::High) {
				if (e_animateIndex == 0)
					e_shape->setTexture(e_revHighTexture_1);
				else
					e_shape->setTexture(e_revHighTexture_2);
			}
			else if (e_level == Level::Low)
			{
				if (e_animateIndex == 0)
					e_shape->setTexture(e_revLowTexture_1);
				else
					e_shape->setTexture(e_revLowTexture_2);
			}
		}
	}
}