#include "button.hpp"

namespace el
{
	Button::Button(int x, int y)
	{
		e_x = x;
		e_y = y;
		
	}
	Button::~Button()
	{

	}

	bool Button::Setup()
	{
		if (!e_textureButton_1.loadFromFile("img/button_1.png") ||
			!e_textureButton_2.loadFromFile("img/button_2.png"))
		{
			std::cout << "ERROR: one of the pictures with the water level was not found" << std::endl;
			return false;
		}

		e_button = std::make_shared<sf::RectangleShape>(sf::Vector2f(300, 100));
		e_button->setPosition(e_x, e_y);

		return true;
	}

	void Button::SetColorRed() { e_button->setTexture(&e_textureButton_2); }
	void Button::SetColorGreen() { e_button->setTexture(&e_textureButton_1); }
}