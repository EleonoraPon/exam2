#include "pump.hpp"

namespace el
{
	Pump::Pump(int x, int y) : Object(x, y)
	{

	}
	Pump::~Pump()
	{

	}

	bool Pump::Setup()
	{
		if (!e_texture.loadFromFile("img/pump.png")  /* && !e_texture.loadFromFile("img/pump1.png") && !e_texture.loadFromFile("img/pump1.png") && !e_texture.loadFromFile("img/pump1.png") */)
		{
			std::cout << "ERROR when loading pump.png" << std::endl;
			return false;
		}

		e_texture.setSmooth(true);
		e_shape = std::make_shared <sf::Sprite>();//������� ���������
		e_shape->setTexture(e_texture);//
		e_shape->setOrigin(static_cast<float>(e_texture.getSize().x / 2), static_cast<float>(e_texture.getSize().y / 2));//static_cast<float> ����������� �������� � ������ ���
																					//���������� ����� ������ (� �����) 
		e_shape->setPosition(e_x, e_y);//����� ������������ ��������


		return true;
	}

	void Pump::Move(int timeMs)
	{
		if (timeMs % 20 == 0)
		{
			e_angle += 1;
			e_shape->setRotation(e_angle);
		}
	}
}