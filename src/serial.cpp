#include <serial.hpp>

namespace el
{
	Serial::Serial(std::string port) : m_serial(m_io)
	{
		try
		{
			m_serial.open(port);
		}
		catch (asio::system_error& e)
		{
			std::cerr << e.what() << std::endl;
		}

	}

	Serial::~Serial()
	{
		try
		{
			m_serial.close();
		}
		catch (asio::system_error& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}

	void Serial::Update(Constituents& constituents)
	{
		try
		{
			// ��������� ������
			//printf("pump %d\n", constituents.pumpwork);
			if (constituents.pumpwork && m_send)
			{
				char buf[1] = { 'a' };
				asio::write(m_serial, asio::buffer(buf));
				m_send = false;
			}
			else if (!constituents.pumpwork && !m_send)
			{
				char buf[1] = { 'b' };
				asio::write(m_serial, asio::buffer(buf));
				m_send = true;
			}

			char data[1];
			size_t nread = asio::read(m_serial, asio::buffer(data, 1));

			printf("%c\n", data[0]);

			// �������� ������ �����
			if (data[0] == 'c')//������� 1
			{
				constituents.res1_high = true;
				constituents.res1_low = false;
			}
			else if (data[0] == 'd')//������ 1
			{
				constituents.res1_high = false;
				constituents.res1_low = true;
			}
			else if (data[0] == 'e')//������� 2
			{
				constituents.res2_high = true;
				constituents.res2_low = false;
			}
			else if (data[0] == 'f')//������ 2
			{
				constituents.res2_high = false;
				constituents.res2_low = true;
			}
		}
		catch (asio::system_error& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
}